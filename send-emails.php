<!-- send-emails.php -->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="custom/images/databass.png">

    <title>Databass Library</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="custom/css/signin.css" rel="stylesheet">
    <script src="js/jquery-1.10.2.js"></script>

    <!-- PHP Files for reporting errors and sql functions -->
    <?php 
    	include "report_errors.php";
	  	include "php/sql-functions.php"; 
  	?>
  </head>

<body>

	<div class="container">
		<?php
            //Get all POST variabls
			$username = $_POST["member_uname"];
        	$password = $_POST["member_pwd"];
        ?>

		<form method="POST" action="login.php">
	        <input type="hidden" name="username" value= <?php echo $username; ?> >
	        <input type="hidden" name="password" value= <?php echo $password; ?> >
			<button class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back to Home </button>
		</form>


		<?php 
			if($db_conn){			

				$result = executePlainSQL("select DISTINCT m.USERNAME from Makes_Rental mr, Members m, Rental_Due_On r, Has_Books h
									where mr.rental_id = r.rental_id AND m.member_id = mr.member_id AND r.isbn = h.isbn AND r.branch_id = h.branch_id AND 
									r.due_date < (select CURRENT_TIMESTAMP from DUAL)");

				oci_fetch_all($result, $row);

				$got_overdue = false;

                if (isset($row["USERNAME"][0]) && (count($row["USERNAME"][0]) >= 1)){
                	$got_overdue = true;

                	$body = "";

                    for($i=0; $i < count($row["USERNAME"]) ; $i++ ) {
                    	$body = $body . "<h4>Email Sent To <b>{$row["USERNAME"][$i]} </b><span class='glyphicon glyphicon-check'></span><h4><br>";
                    }

                }

				//Commit changes
				logoff_oci();
			}
			else{
				echo "cannot connect";
				$e = OCI_Error(); // For OCILogon errors pass no handle
				alert(htmlentities($e['message']));
			}
		?>

			<?php if ($got_overdue) { ?>
			<div class="jumbotron text-center" style="padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
			
			<h2>Sending <b><?php echo count($row["USERNAME"]) ;?></b> Emails</h2>
			
			<hr>
			
			<?php echo $body; ?>

		</div>

		<?php } else { ?>

		<div class="alert alert-warning">No Overdue Books. No Emails to Send :)</div>

		<?php } ?>


	</div> <!-- End of container div-->

</body>
</html>