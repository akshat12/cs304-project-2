<!-- delete_rental_information.php -->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="custom/images/databass.png">

    <title>Databass Library</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<style type="text/css">
		.modal-dialog {
		  width: auto;
		  height: auto;
		  padding: 0;
		}

		.modal-content {
		  height: auto;
		  border-radius: 0;
		}
    </style>

    <!-- Custom styles for this template -->
    <link href="custom/css/signin.css" rel="stylesheet">
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
    	$(window).on('load',function(){
    		$("#show_modal_btn").on("click",function(){
    			$("#myModal").modal();
    		}); 
    	});

    </script>

    <!-- PHP Files for reporting errors and sql functions -->
    <?php 
    	include "report_errors.php";
	  	include "php/sql-functions.php"; 
  	?>
  </head>

  <!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Showing Database Tables</h4>
	      </div>
	      <div class="modal-body">
	        <table class="table" id="db-table">
	        </table>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<?php
	// Get all the POST variables
		$username = $_POST["member_uname"];
		$password = $_POST["member_pwd"];
		$member_id = $_POST['member_id'];
		$rental_id = $_POST['rental-delete-query-text'];
	?>


 
<body>
	<div class="container">
		<form method="POST" action="login.php">
	        <input type="hidden" name="username" value= <?php echo $username; ?> >
	        <input type="hidden" name="password" value= <?php echo $password; ?> >
			<button class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back to Home </button>
		</form>

		<hr>

        <div class="jumbotron text-center" style="padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
        	<!-- PHP Script for deleting rentals -->
			 <?php 
			  		function delete_info($id, $conn){
						$query = "DELETE FROM Rental_due_on R WHERE R.rental_id=";
						$stmt = $query . $id;
						$s = oci_parse($conn, $stmt);
						$r = oci_execute($s);

						return $r;
					}

			  		if($db_conn){
			  			//Option for delete by rental id has been selected
						$result = executePlainSQL("SELECT COUNT(DISTINCT R.rental_id)  AS NUMENTRIES FROM RENTAL_DUE_ON R WHERE R.rental_id = ". $rental_id);
						oci_fetch_all($result, $row);

						$num_entries = intval($row["NUMENTRIES"][0]);
						$rental_info_exists = ($num_entries == 1)? true : false ; //Only one row should exist for the given rental_id since it is a primary key of Rental_due_on

						if($rental_info_exists){
							//Get the information for the rental such as the ISBN and Copy ID
							$result = executePlainSQL("SELECT *  FROM RENTAL_DUE_ON R WHERE R.rental_id = ". $rental_id);
							oci_fetch_all($result, $row);
							$bool_result = delete_info($rental_id, $db_conn);
							$oci_commit = OCICommit($db_conn);
							
				        	echo "<h2> Deleting Information For Rental ID : {$rental_id} </h2> <hr>";

							if ($bool_result) {
								// echo "<h3 style='color:#4cae4c;'>Successfully Deleted The Rental With The Following Information :</h3>";
								echo "<div class='alert alert-success'>Successfully Deleted The Rental With The Following Information :";
								echo "<h4>ISBN # : " . $row["ISBN"][0] . "</h4>";
								echo "<h4>COPY # : " . $row["COPY_ID"][0] . "</h4></div>";
							} else {
								echo "<div class='alert alert-danger'>There was some problem in deleting the information :(</div>";
							}
			  			}
			  			else{ //Rental Info for the given rental id does not exist
			  				echo "<div class='alert alert-warning'>Rental ID : {$rental_id} does not exist</div>";
			  			}
			  			// Display Tables After Delete
						$tableToDisplay = getTable("RENTAL_DUE_ON");
						echo "<script>$('#db-table','.modal-body').append('{$tableToDisplay}')</script>";
						$tableToDisplay = getTable("MAKES_RENTAL");
						echo "<script>$('#db-table','.modal-body').append('<b><p>Database Table MAKES_RENTAL</p></b>')</script>";
						echo "<script>$('#db-table','.modal-body').append('{$tableToDisplay}')</script>";
						echo "<button class='btn btn-primary btn-lg' id='show_modal_btn' data-toggle='modal' data-target='#myModal'>Show Database Table For Rental_Due_On</button>";

			  			logoff_oci();
			  		}
			  		else{
			  			echo "<div class='alert alert-error'>Error Connecting to Database :(. Please Try Again Later. </div>";
			  		}
			  ?>
        </div>


	</div> <!-- End of container div-->

</body>
</html>