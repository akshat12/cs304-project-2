<?php 
	include "report_errors.php";
	include "php/sql-functions.php"; 

	//Do not use intval when using isbn #'s'

	// Do not forget to use json_encode when returning the result

	if($db_conn) {
		$isbn = $_POST['isbn'];
		$branch_id = $_POST['branch_id'];
		$username = $_POST['username'];
		$member_id = $_POST['member_id']; //May not need this.


		//Get the last row from the Reservation_for table
		$result = executePlainSQL("SELECT RESERVATION_ID FROM RESERVATION_FOR");
		oci_fetch_all($result, $row);

		$arr = array();

		foreach ($row["RESERVATION_ID"] as $key ) {
			array_push($arr, intval($key));
		}

		$reservation_id = 1 + max($arr);

		$query = "INSERT INTO Reservation_for VALUES('{$reservation_id}','{$isbn}','{$branch_id}', (select CURRENT_TIMESTAMP from DUAL))";
		executePlainSQL($query);

		$oci_commit = OCICommit($db_conn);

		$query2 = "INSERT INTO Makes_Reservation VALUES('{$member_id}','{$reservation_id}')";
		executePlainSQL($query2);

		$oci_commit = OCICommit($db_conn);

		$return_data = array();

		if(getSuccessStatus()){
			$return_data["status"] = "okay";
		}
		else{
			$return_data["status"] = "error";
		}

		echo json_encode($return_data);

		//Commit changes
		logoff_oci();

	}

?>