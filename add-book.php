<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="custom/images/databass.png">

    <title>Databass Library</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    
    <!-- Custom styles for this template -->
    <link href="custom/css/signin.css" rel="stylesheet">


    <!-- JavaScript Scripts and Files -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- PHP Files for reporting errors and sql functions -->
    <?php 
    	include "report_errors.php";
	  	include "php/sql-functions.php"; 
  	?>
  </head>

<body>
	<div class="container">

	<!-- Any Errors that pop up will be shown within the container but outside the jumbotron -->
	<?php 
		$username = $_POST["username"];
		$password = $_POST["password"];
		$isbn = $_POST["isbn"];
		$title = $_POST["title"];
		$publisher = $_POST["publisher"];
		$author = $_POST["author"];
		$branch_id = $_POST["branch-id"];
		$copy_id = $_POST["copy-id"];

		executePlainSQL("INSERT INTO Has_Books VALUES('{$isbn}','{$branch_id}','{$publisher}','{$title}','{$author}')");
		$oci_commit = OCICommit($db_conn);

		executePlainSQL("INSERT INTO Book_Copy VALUES('{$copy_id}', '{$isbn}', '{$branch_id}')");
		$oci_commit = OCICommit($db_conn);

		$result_bool = getSuccessStatus();

		//Commit changes
		logoff_oci();
	?>

		<div class="jumbotron text-center" style="padding-left: 10px; padding-top: 10px; padding-bottom: 10px; background-color: #DDDDDD;">
		<!-- On success commit -->
		<?php if ($result_bool) { ?> 
			<h2> Book Was Inserted Successfully </h2>
			<img src="custom/images/success.png" height="200px" width="200px">
		<?php } else { ?>
			<h2> Error when Inserting Book <img src="custom/images/smiley-sad.png" height="20px" width="20px"> </h2>

			<form method="POST" action="insert-book.php">
		        <input type="hidden" name="member_uname" value= <?php echo $username; ?> >
		        <input type="hidden" name="member_pwd" value= <?php echo $password; ?> >
		        <button class="btn btn-danger btn-large">Try Again</button>
			</form>
		<?php } ?>
			<form method="POST" action="login.php">
		        <input type="hidden" name="username" value= <?php echo $username; ?> >
		        <input type="hidden" name="password" value= <?php echo $password; ?> >
				<button class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back to Home </button><br>
			</form>
		</div>

	</div> <!-- End of container div-->

</body>
</html>