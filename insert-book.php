<!-- insert-book.php -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="custom/images/databass.png">

    <title>Databass Library</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="custom/css/signin.css" rel="stylesheet">
    <script src="js/jquery-1.10.2.js"></script>

    <!-- PHP Files for reporting errors and sql functions -->
    <?php 
    	include "report_errors.php";
	  	include "php/sql-functions.php"; 
  	?>
  </head>

<body>
	<div class="container">

		<?php 
			$username = $_POST["member_uname"];
        	$password = $_POST["member_pwd"];
		?>

		<form method="POST" action="login.php">
	        <input type="hidden" name="username" value= <?php echo $username; ?> >
	        <input type="hidden" name="password" value= <?php echo $password; ?> >
			<button class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back to Home </button>
			<hr>
		</form>

        <div class="jumbotron text-center" style="padding-left: 10px; padding-top: 10px; padding-bottom: 10px; background-color: #DDDDDD;">
        <!-- Make the form with the user details filled in -->
        	
        	<form method="POST" action="add-book.php" class="form-signin">
                <input type="hidden" name="username" value= <?php echo $username; ?> >
                <input type="hidden" name="password" value= <?php echo $password; ?> >

        		<h2 class="form-signin-heading">Insert a new book</h2>
        		<hr>

        		<h4>ISBN</h4>
        		<input type="text" name="isbn" class="form-control" placeholder="ISBN" maxlength="13" required autofocus>

        		<h4>Title</h4>
        		<input type="text" name="title" class="form-control" placeholder="Book Title" maxlength="80" required autofocus>

        		<h4>Publisher</h4>
        		<input type="text" name="publisher" class="form-control" placeholder="Publisher" maxlength="35" required autofocus>

        		<h4>Author</h4>
        		<input type="text" name="author" class="form-control" placeholder="Author" maxlength="20" required autofocus>

        		<h4>Branch ID</h4>
        		<input type="text" name="branch-id" class="form-control" placeholder="Branch ID" maxlength="4" required autofocus>

        		<h4>Copy ID</h4>
        		<input type="text" name="copy-id" class="form-control" placeholder="Copy ID" maxlength="4" required autofocus>

        		<hr>
        		<button class="btn btn-lg btn-primary btn-block" style="opacity:0.9;"type="submit">Insert Book</button>
      		</form>    

        </div>

	</div> <!-- End of container div-->

</body>
</html>