<!-- show-reservations.php -->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="custom/images/databass.png">

    <title>Databass Library</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="custom/css/signin.css" rel="stylesheet">
    <script src="js/jquery-1.10.2.js"></script>

    <!-- PHP Files for reporting errors and sql functions -->
    <?php 
    	include "report_errors.php";
	  	include "php/sql-functions.php"; 
  	?>
  </head>

<body>

	<div class="container">
		<?php
            //Get all POST variabls
			$username = $_POST["member_uname"];
        	$password = $_POST["member_pwd"];
            $member_id = $_POST["member_id"];
        ?>

		<form method="POST" action="login.php">
	        <input type="hidden" name="username" value= <?php echo $username; ?> >
	        <input type="hidden" name="password" value= <?php echo $password; ?> >
			<button class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back to Home </button>
		</form>


		<?php 
			if($db_conn){			

				$result = executePlainSQL("select r.ISBN, r.RESERVATION_DATE from RESERVATION_FOR r ,MAKES_RESERVATION m where r.reservation_id = m.reservation_id AND m.member_id =". $member_id ."");

				oci_fetch_all($result, $row);

				$reservations_made = false;

                if (isset($row["ISBN"][0]) && (count($row["ISBN"][0]) >= 1)){
                    $reservations_made = true;
                    $thead = "<thead><tr><th>ISBN</th><th>RESERVATION_DATE</th></tr></thead>";
                    $tbody = "<tbody>";

                    for($i=0; $i < count($row["ISBN"]) ; $i++ ) {
                    	$tbody = $tbody . "<tr><td>{$row["ISBN"][$i]}</td><td>{$row["RESERVATION_DATE"][$i]}</td></tr>";
                    }

                    $tbody = $tbody . "</tbody>";
                }

				//Commit changes
				logoff_oci();
			}
			else{
				echo "cannot connect";
				$e = OCI_Error(); // For OCILogon errors pass no handle
				alert(htmlentities($e['message']));
			}
		?>

		<div class="jumbotron text-center" style="padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
			<?php if ($reservations_made) { ?>
			<h2>Showing All Reservations</h2>
			
			<hr>
			
			<table class='table' style='text-align:left;'><?php echo ( $thead . $tbody )?></table>

			<?php } else { ?>

			<div class="alert alert-warning">You have no reservations</div>

			<?php } ?>

		</div>


	</div> <!-- End of container div-->

</body>
</html>